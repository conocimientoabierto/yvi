import axios from 'axios'
import * as d3 from 'd3'

export default class YQS {
  constructor () {
    /*
      On created ...
    */
    this.config = null;
    this.country = null;
    this.election = null;
    this.questions = null;
    this.candidates = null;
    this.fakes_all = [];
    this.questionIndexes = [];
    this.propuestas = false;
    this.store;
  }

  init (store) {
    if (store)
      this.store = store;
    /*
      Get the config file fron the static dir
    */
    return new Promise((resolve, reject) => {
      axios.get('/config.json')
        .then(response => {
          this.config = response.data
          resolve('Done')
        })
        .catch(error => {
          reject(Error(error))
        })
    })
  }

  getCandidatesByCountry (countryName) {
    /*
      Given some country, get all the candidates json files and cache the
      photos of the candidates.

      TODO
      ----
      * Mostrar un mejor error cuando el url es invalido
      * Si tengo muchas elecciones no lo va a resolver correctamente
    */
    this.country = null
    this.candidates = {}

    // Filter by countryName
    let filter = this.config.filter(c => c.country_name === countryName)

    // If not country with this name, return reject
    if (filter.length == 0) {
      return new Promise((resolve, reject) => {
        reject(Error('Invalida counytry name'))
      })
    }
    else {
      this.country = filter[0]
    }

    // Set default values
    this.country.elections = this.country.elections.map(election => {
      election.shuffle = typeof election.shuffle === 'undefined' ? true : election.shuffle;
      election.hide_canditates = typeof election.hide_canditates === 'undefined' ? false : election.hide_canditates;

      return election
    })

    let promises = []
 //   let urls = []

    this.country.elections.forEach(election => {
   //   urls.push(axios.get(election.candidates_url, {election: election.name}))

      promises.push(
        d3.csv(election.candidates_url, (d) => {
          let l = []
          let sources = {}
         // console.log(JSON.stringify(d))
          for (let key in d) {
            if (d.hasOwnProperty(key) && /q_[0-9]+$/.test(key)) {
              let index = parseInt(key.split('_')[1]); //- 1;
              if (!this.questionIndexes.includes(index))
                this.questionIndexes.push(index);
              l.push(index)
            }
            if (d.hasOwnProperty(key) && /Link_fuente_[0-9]+$/.test(key)) {
          //    console.log(key)
              let index = parseInt(key.split('_')[2]); //- 1;
          //    console.log(index)
              sources[index] = d['Link_fuente_'+index]
            }
          }

          let amountElements = Math.max.apply(Math, l)
          let sortAnswer = new Array(amountElements)
          let longAnswer = new Array(amountElements)

          for (let key in d) {
            if (d.hasOwnProperty(key) && /q_[0-9]+$/.test(key)) {
              let index = parseInt(key.split('_')[1]);// - 1;
              let answer = parseInt(d[key]);
              sortAnswer[index] = (isNaN(answer) ? -1 : answer);
            //  if (isNaN(answer) && d.Candidato != 'vicepresidente') {
            //    console.log(' C:'+d.last_name+ ' P: '+index+' -> '+answer+ '('+d[key]+')');
             // }
            }
          }

          for (let key in d) {
            if (d.hasOwnProperty(key) && /q_[0-9]+_/.test(key)) {
              let index = parseInt(key.split('_')[1]);// - 1;
              longAnswer[index] = d[key];
            }
          }

          return {
            propuestas: d.propuestas,
            distrito: d.distrito ? d.distrito : null,
            Candidato: d.Candidato,
            biography: d.biography,
            birthday: d.birthday,
            photo: d.photo_link? d.photo_link : d.photo,
            photo_link: d.photo_link,
            color: d.color,
            email: d.email,
            instagram: d.instagram,
            facebook: d.facebook,
            twitter: d.twitter,
            web_site: d.web_site,
            first_name: d.first_name,
            last_name: d.last_name,
            full_name: (d.first_name + ' ' + d.last_name).replace(/ /g, '-'),
            list: d.list,
            party_logo: d.party_logo,
            politic_party: d.politic_party,
            answers_pdf: d.answers_pdf,
            answer_excel: d.answer_excel,
            sort_answer: sortAnswer,
            long_answer: longAnswer,
            sources: sources
          }
        }).then(candidates => {
          if (election.shuffle) {
            candidates = shuffleArray(candidates)
          }

          this.candidates[election.name] = candidates;
         // cacheCandidatePhoto(candidates)
        }).catch((error) => {
          console.log(`Error on election: ${election.name}. Error: ${error.message}`)
          return error.message
        })
      )
    })

    return Promise.all(promises)
      .then(results => { Promise.resolver('Done') })
      .catch(() => { })
  }

  getQuestionsByElection (electionName) {
    /*
      Given some country and election, get the question file
    */
    this.election = null
    this.questions = null

    // If not country with this name, return reject
    if (this.country === null) {
      return new Promise((resolve, reject) => {
        reject(Error('Invalid country name'))
      })
    }

    this.election = this.getElection(electionName);

    // If not country with this name, return reject
    if (this.election === null) {
      return new Promise((resolve, reject) => {
        reject(Error('Invalid election name'))
      })
    }

    return new Promise((resolve, reject) => {

      d3.csv(this.election.questions_url)
        .then((data) => {
          // Add the question original index
          //data.forEach((question, index) => {
          //  question.originalIndex = index;
          //});
          for (let qd = 0; qd < data.length; qd++ ) {
            if (!data[qd].pindex) {
              data[qd].originalIndex = qd;
              data[qd].pindex = qd;
            }
          }
          //console.log(this.election);
          let lquestions = {};
          let questionsarray = randomSubArray(data, this.election.questions_number);
          for (let q = 0; q < questionsarray.length; q++ ) {
            if (questionsarray[q].pindex)
              lquestions[questionsarray[q].pindex] = questionsarray[q];
            else
              lquestions[questionsarray[q].originalIndex] = questionsarray[q];
          }

          let allquestions = {};
          for (let q = 0; q < data.length; q++ ) {
            if (data[q].pindex)
              allquestions[data[q].pindex] = data[q];
            else
              allquestions[data[q].originalIndex] = data[q];
          }
          this.questions = lquestions;
          this.questions_all = data;
/*
          // Agrego la propiedad more_options en yqs.questions
          if (this.election.hasOwnProperty('more_options')) {
            this.questions.more_options = this.election.more_options
          }
          else {
            this.questions.more_options = true;
          }

          // Agrego el nombre de las columnas de las Respuestas
          let optionsNames = data.columns.slice(2);
          optionsNames.push('no_answer');
          this.questions.options_names = optionsNames;
*/

          resolve('Done')
        })
        .catch(error => {
          console.log(error);
          //this.errors.push(error);
          reject(Error(error));
        })
    })
  }

  getFakesByElection (electionName) {
    /*
      Given some country and election, get the question file
    */
    this.election = null;
    this.questions = null;

    // If not country with this name, return reject
    if (this.country === null) {
      return new Promise((resolve, reject) => {
        reject(Error('Invalid country name'))
      })
    }

    this.election = this.getElection(electionName);

    // If not country with this name, return reject
    if (this.election === null) {
      return new Promise((resolve, reject) => {
        reject(Error('Invalid election name'))
      })
    }

    return new Promise((resolve, reject) => {
      d3.csv(this.election.fakes_url)
          .then((data) => {
            // Add the question original index
            data.forEach((question, index) => {
              question.originalIndex = index
            });
            const filtered = data.filter(function(element){
              return element.imagen && element.verificacion && element.imagen !== '' && element.verificacion !== '';
            });
            this.fakes_all = filtered;
            //console.log(data);
            resolve('Done')
          })
          .catch(error => {
            this.errors.push(error)
            reject(Error(error))
          })
    })
  }

  getElection (electionName) {
    this.election = null;
    this.questions = null;

    // If not country with this name, return reject
    if (this.country === null) {
      return new Promise((resolve, reject) => {
        reject(Error('Invalid country name'))
      })
    }

    // Filter by electionName
    for (var i = 0; i < this.country.elections.length; i++) {
      if (this.country.elections[i]['name'] === electionName) {
        this.election = this.country.elections[i]
      }
    }

    // If not country with this name, return reject
    if (this.election === null) {
      return {}
    }
    return this.election;
  }

  hasGame (gameName) {
    if (this.election.hasOwnProperty('games')) {
      if (this.election.games.includes(gameName))
        return true
      else
        return false
    }
    else {
      return true;
    }
  }

  getPropuestasByCountry (countryName, electionName) {
    /*
      Given some country, get all the candidates json files and cache the
      photos of the candidates.

      TODO
      ----
      * Mostrar un mejor error cuando el url es invalido
      * Si tengo muchas elecciones no lo va a resolver correctamente
    */
    //this.country = null
    let vm = this;
    vm.propuestas = {};

    // Filter by countryName
    let filter = vm.config.filter(c => c.country_name === countryName)

    // If not country with this name, return reject
    if (filter.length == 0) {
      return new Promise((resolve, reject) => {
        reject(Error('Invalida counytry name'))
      })
    }
    else {
      this.country = filter[0]
    }



    if (!this.propuestas[countryName])
      this.propuestas[countryName] = {};
    // Set default values
    this.country.elections = vm.country.elections.map(election => {
      election.shuffle = typeof election.shuffle === 'undefined' ? true : election.shuffle;
      election.hide_canditates = typeof election.hide_canditates === 'undefined' ? false : election.hide_canditates;
      return election
    });

    let promises = [];

    this.country.elections.forEach(election => {
      if (election.propuestas_url) {
        //urls.push(axios.get(election.propuestas_url, {election: election.name}))
        if (election.name == electionName) {
          vm.election = election;
          if (vm.store) {
            vm.store.commit('main/setElection', vm.election)
          }
  //        console.log('Pais '+countryName+' eleccion '+election.name+' '+vm.election.name);
        } else {
  //        console.log('Pais '+countryName+' !eleccion '+election.name+' '+electionName);
        }
      }
    });

    if (vm.store) {
      vm.store.commit('main/setCountry', Object.assign(this.country, {}))
    }

 //   console.log('Pais '+countryName+' eleccion '+vm.election.name);
    if (vm.election) {
 //     console.log(`Propuestas Fetching ${vm.election.name} - ${countryName}`);
    return d3.csv(vm.election.propuestas_url)
      .then(function (propuestas)  {
  //            console.log(`Propuestas Fetched ${vm.election.name} - ${countryName}`);
              console.log(propuestas);
              if (vm.store) {
     //           console.log(vm.store);
                vm.store.commit('main/setPropuestas', propuestas)
              }
              vm.propuestas[countryName][vm.election.name] = propuestas;
              return true;
            })
      .catch((error) => {
              console.log(`Error on propuestas: ${vm.election.name}. Error: ${error.message}`)
              return error.message
      });
    }
    else {
      console.log(`Propuestas NOT Fetched ${election} - ${countryName}`);
      return Promise.resolve();
    }
  }
}

function shuffleArray(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array
}

function randomSubArray (arr, size) {
  /*
    Return N (size) element from a array
  */
  var shuffled = arr.slice(0)
  var i = arr.length
  var temp
  var index

  while (i--) {
    index = Math.floor((i + 1) * Math.random())
    temp = shuffled[index]
    shuffled[index] = shuffled[i]
    shuffled[i] = temp
  }
  return shuffled.slice(0, size)
}

function cacheCandidatePhoto (candidates) {
  /*

  */
  let urls = []
  candidates.forEach(candidate => {
    urls.push(axios.get(candidate.photo))
  })

  // Process all candidates files
  axios.all(urls)
    .then(axios.spread((...args) => {
      // Both requests are now complete
      for (let i = 0; i < args.length; i++) {

      }
    }))
}

function calculateSimilarity (candidates, userAnswer) {
  // Calculate the similarity to each other candidate
  let similar = []

  for (let i = 0; i < candidates.length; i++) {
    let r = []

    for (let j = 0; j < userAnswer.length; j++) {
      let a = userAnswer[j]
      let b = candidates[i].sort_answer[j] ? candidates[i].sort_answer[j] : 'X';

      if ([0, 1].includes(a) && [0, 1].includes(b)) {
        r.push(1)
      } else if ([2, 3].includes(a) && [2, 3].includes(b)) {
        r.push(1)
      }
      else {
        r.push(0)
      }
    }

    let similarity = r.reduce((a, b) => a + b, 0) / r.length * 100;

    if (candidates[i].Candidato == 'presidente')
    similar.push({
      // similarity: similarity.toFixed(2),
      similarity: similarity,
      name: candidates[i].full_name,
      photo: candidates[i].photo,
      party: candidates[i].politic_party
    })
  }

  // sort
  similar.sort((a, b) => b.similarity - a.similarity);
  return similar
}


function calculateSimilarity_nuevo (candidates, userAnswer, respuestas, questions) {
  // Calculate the similarity to each other candidate
  let similar = [];

  for (let i = 0; i < candidates.length; i++) {
    let r = [];
  //  console.log('Candidato: '+candidates[i].last_name);

    respuestas.forEach((respuesta, index)  => {
      //console.log(respuesta, index);
      let a = respuesta.rta;
      let b = candidates[i].sort_answer[respuesta.pindex];
    //  console.log('Pregunta Original ('+respuesta.pindex+'): '+questions[respuesta.pindex].question+' J: '+respuesta.rta+' C: '+candidates[i].sort_answer[respuesta.pindex]);
    //  console.log('Pregunta Rta: '+            a                            +' J: '+respuesta.rta+' C: '+b);

      //console.log('Pregunta: '+questions[respuesta.pindex]+' Para la respuesta ('+a+') '+index+' '+  candidates[i].last_name + ' respondio '+b );
      //console.log('Pregunta: '+questions[respuesta.pindex]+' Para la respuesta ('+a+') '+index+' '+  candidates[i].last_name + ' respondio '+b );
      if ([0, 1].includes(a) && [0, 1].includes(b)) {
        //console.log('Para la respuesta ('+a+') '+index+' '+  (candidates[i].name ? candidates[i].name.replace(/-/g, ' ')  : candidates[i])+ ' respondio '+b );
      //  console.log('match por si');
        r.push(1)
      }
      else if ([2, 3].includes(a) && [2, 3].includes(b)) {
      //  console.log('match por no');
        r.push(1)
      }
      else {
       // console.log('Sin match');
        r.push(0)
      }
     // console.log('---------------------Fin check respuesta');
    });

    /*for (let j = 0; j < userAnswer.length; j++) {
      let a = userAnswer[j]
      let b = candidates[i].sort_answer[j] ? candidates[i].sort_answer[j] : 'X';

      if ([0, 1].includes(a) && [0, 1].includes(b)) {
        r.push(1)
      } else if ([2, 3].includes(a) && [2, 3].includes(b)) {
        r.push(1)
      }
      else {
        r.push(0)
      }
    }*/

    let similarity = r.reduce((a, b) => a + b, 0) / r.length * 100;

  //  if (candidates[i].Candidato == 'presidente')
      similar.push({
        // similarity: similarity.toFixed(2),
        similarity: similarity,
        name: candidates[i].full_name,
        photo: candidates[i].photo,
        party: candidates[i].politic_party
      });
   //console.log('---------------------Fin check candidato');
 }

  // sort
  similar.sort((a, b) => b.similarity - a.similarity);
  return similar
}

export { YQS, calculateSimilarity, shuffleArray, randomSubArray, calculateSimilarity_nuevo }
