import Vue from 'vue'
import VueCarousel from 'vue-carousel'

// Setup Carousel
Vue.use(VueCarousel)
