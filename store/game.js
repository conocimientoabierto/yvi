export const state = () => ({
  mode: 'menu',
  actualquestion: 0,
  totalQuestions: 5
})

export const mutations = {
  selectMode (state, mode) {
    state.mode = mode
  },
  nextQuestion (state, mode) {
    state.actualquestion++;
  },
  settotalQuestions (state, mode) {
    state.totalQuestions = mode;
  }
}
