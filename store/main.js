export const state = () => ({
    config: null,
    country: null,
    election: null,
    questions: null,
    candidates: null,
    fakes_all: [],
    questionIndexes: [],
    propuestas: false
});

export const mutations = {
  setPropuestas (state, mode) {
    state.propuestas = mode
  },
  setElection (state, mode) {
    state.election = mode;
  },
  setCountry (state, mode) {
    state.country = mode;
  }
}
