import pkg from './package'
import { resolve } from 'path'

export default {
  mode: 'spa',

  alias: {
    'imgs': resolve(__dirname, './static/imgs')
  },

  fontawesome: {
    component: 'fa'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Yo Voto Informado',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },

      // Twitter card
      { name: 'twitter:card', content: 'summary' },
      { name: 'twitter:site', content: '@yovotoinfo' },

      // Facebook OG
      { property: 'og:title', content: 'Yo Voto Informado' },
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: 'https://yovotoinformado.org/' },
      { property: 'og:image', content: '/logo-og.png' },
      { property: 'og:description', content: `Yo Voto Informado es una aplicación interactiva
      con el objetivo de lograr un electorado informado en procesos electorales
      en cada país de América Latina` }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/icon/favicon_32x32.ico' },
      { rel: 'apple-touch-icon-precomposed', size: '144x144', type: 'image/x-icon', href: '/icon/favicon_144x144.ico' },
      { rel: 'apple-touch-icon-precomposed', size: '114x114', type: 'image/x-icon', href: '/icon/favicon_114x114.ico' },
      { rel: 'apple-touch-icon-precomposed', size: '72x72', type: 'image/x-icon', href: '/icon/favicon_72x72.ico' },
      { rel: 'apple-touch-icon-precomposed', type: 'image/x-icon', href: '/icon/favicon_57x57.ico' }
    ],
    script: [
      { src: 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0&autoLogAppEvents=1' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/main.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vue-carousel',
    '~/plugins/vue-hammer'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',

    // Matomo
   /* ['nuxt-matomo', {
      matomoUrl: '//analytics.conocimientoabierto.org/',
      siteId: 1
    }],
*/
    // Font Awesome
    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['faUserFriends', 'faTrophy', 'faChartLine', 'faHome','faLink','faArrowRight']
        },
        {
          set: '@fortawesome/fontawesome-free-brands',
          icons: ['faFacebookF', 'faTwitter', 'faInstagram', 'faGitlab']
        }
      ]
    }],

    // i18n
    ['nuxt-i18n', {
      locales: [
        { code: 'es', iso: 'es-ES', name: 'Español' },
        { code: 'br', iso: 'pt-PT', name: 'Português' },
        { code: 'en', iso: 'en-US', name: 'English' }
      ],
      defaultLocale: 'es',
      vueI18n: {
        fallbackLocale: 'es',
        messages: {
          es: require('./lang/es.json'),
          br: require('./lang/br.json'),
          en: require('./lang/en.json')
        }
      },

      // Define the url acording the lang
      parsePages: false,
      pages: {
        'methodology': {
          es: '/metodologia',
          br: '/metodologia',
          en: '/methodology'
        },
        'we': {
          es: '/nosotros',
          br: '/nos',
          en: '/we'
        },
        '_country/_election/candidate/_name': {
          es: '/:country/:election/candidato/:name',
          br: '/:country/:election/candidato/:name',
          en: '/:country/:election/candidate/:name'
        },
        '_country/_election/game': {
          es: '/:country/:election/juego',
          br: '/:country/:election/jogar',
          en: '/:country/:election/game'
        },
        '_country/_election/juego/_juego': {
          es: '/:country/:election/juego/:juego',
          br: '/:country/:election/juego/:juego',
          en: '/:country/:election/juego:/juego'
        }
      }
    }],
    ['@nuxtjs/google-analytics', {
      id: 'UA-180151411-1'
    }]
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },
  buildModules: [
    '@nuxtjs/google-analytics'
  ],
  googleAnalytics: {
    id: 'UA-180151411-1',
    debug: {
      enabled: true,
      sendHitTask: true
    }
  }

}
