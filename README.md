# yvi

> My supreme Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

* 0 es si
* 1 es si, pero
* 2 es no
* 3 es no, pero

## Doc

* Tamano de las banderass: 88x88 px
* Orden random de los candidatos al comienzo

## TODO

- [ ] Usar variables para los colores base del CSS
- [ ] Pasar las fonts en los 3 formatos
- [x] Los url tambien se deberian traducir
- [x] El nombre de la pages en el codigo deberia estar en ingles
- [x] Hacer la version en ingles
- [ ] Mejorar el navbar. Hay parte que es copy and past
- [ ] Las personas de `/nosotros` deberian de estar en un csv externo
- [ ] Lo mismo para los links de las noticias
- [ ] En `/:country` el *faltan tantos dias* o *upps ya paso* tambien hay que traducirlo
- [ ] Buena documentacion
- [ ] En el config general se deberia poder optar que juegos mostrar
- [ ] El url deberia siempre estar en minuscula.
